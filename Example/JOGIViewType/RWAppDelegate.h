//
//  RWAppDelegate.h
//  JOGIViewType
//
//  Created by Jogendra Singh on 12/26/2016.
//  Copyright (c) 2016 Jogendra Singh. All rights reserved.
//

@import UIKit;

@interface RWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
