//
//  main.m
//  JOGIViewType
//
//  Created by Jogendra Singh on 12/26/2016.
//  Copyright (c) 2016 Jogendra Singh. All rights reserved.
//

@import UIKit;
#import "RWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RWAppDelegate class]));
    }
}
