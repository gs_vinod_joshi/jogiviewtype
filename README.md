# JOGIViewType

[![CI Status](http://img.shields.io/travis/Jogendra Singh/JOGIViewType.svg?style=flat)](https://travis-ci.org/Jogendra Singh/JOGIViewType)
[![Version](https://img.shields.io/cocoapods/v/JOGIViewType.svg?style=flat)](http://cocoapods.org/pods/JOGIViewType)
[![License](https://img.shields.io/cocoapods/l/JOGIViewType.svg?style=flat)](http://cocoapods.org/pods/JOGIViewType)
[![Platform](https://img.shields.io/cocoapods/p/JOGIViewType.svg?style=flat)](http://cocoapods.org/pods/JOGIViewType)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

JOGIViewType is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "JOGIViewType"
```

## Author

Jogendra Singh, jogendra.singh@girnarsoft.com

## License

JOGIViewType is available under the MIT license. See the LICENSE file for more info.
